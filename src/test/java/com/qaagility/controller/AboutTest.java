package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.About;
import java.lang.String;

public class AboutTest {
    @Test
    public void testAbout() throws Exception {
        String obj = new About().desc();
        assertEquals("String test", "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", obj);
    }

}

